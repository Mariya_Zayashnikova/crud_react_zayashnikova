 export default class getPostServices  {
    constructor() {
        this._ApiBase = 'http://178.128.196.163:3000/api/records';
    }

    getData = async () => {
        const res = await fetch(this._ApiBase);

        if(!res.ok) {
             throw new Error(`Ошбика по адресу: ${this._ApiBase}, Статус: ${res.status}`); 
        }

        return await res.json();
    }

    postData = async (data) => {
        const res = await fetch(this._ApiBase, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(data)
        });

        if(!res.ok) {
             throw new Error(`Ошбика по адресу: ${this._ApiBase}, Статус: ${res.status}`); 
        }

        return await res.json();
    }

    deleteItem = async (id) => {
        const res = await fetch(`${this._ApiBase}/${id}`, {
            method: 'DELETE'
        });

        if(!res.ok) {
             throw new Error(`Ошбика по адресу: ${this._ApiBase}, Статус: ${res.status}`); 
        }

        return await res.json();
    }

    updateItem = async (id, data) => {
        const res = await fetch(`${this._ApiBase}/${id}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(data)
        });

        if(!res.ok) {
             throw new Error(`Ошбика по адресу: ${this._ApiBase}, Статус: ${res.status}`); 
        }

        return await res.json();
    }
    
}