import React, {Component} from 'react';
import Button from 'react-bootstrap/Button';
import './postAddForm.css';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

export default class PostAddForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            surname: ''
        }
        this.onChangeFirstName = this.onChangeFirstName.bind(this);
        this.onChangeSurName = this.onChangeSurName.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChangeFirstName(event) {
        this.setState({name: event.target.value}); 
    }

    onChangeSurName(event) {
        this.setState({surname: event.target.value}); 
    }

    onSubmit(event) {
        event.preventDefault();
        this.props.addItem(this.state);
        this.setState({
            name: '',
            surname: ''    
        });
        event.target.reset();
    }
    
    render() {
        return (
            <Form
                 className="form"
                 onSubmit={this.onSubmit}
            >
                <Row>
                     <Col>
                        <Form.Control
                             placeholder="First name"
                             onChange={this.onChangeFirstName}
                         />
                    </Col>
                    <Col>
                        <Form.Control
                             placeholder="Last name" 
                             onChange={this.onChangeSurName}
                        />
                    </Col>
                </Row>
                <Button type="submit" variant="primary" className="btn-submit">Добавить запись</Button>
            </Form>
        )
    }
}
