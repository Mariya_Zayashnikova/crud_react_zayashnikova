import React, { Component } from 'react';
import Table from 'react-bootstrap/Table'
import 'bootstrap/dist/css/bootstrap.min.css';
import './Body.css';
import Button from 'react-bootstrap/Button';
import { imgDelete, imgUpdate } from './img/icons-svg';
import getPostServices from '../../services/getPostServices';
import PostAddForm from './postAddForm/postAddForm';
import Form from 'react-bootstrap/Form';
import Alert from 'react-bootstrap/Alert';
import Spinner from 'react-bootstrap/Spinner'


export default class Body extends Component {

    constructor(props) {
        super(props);
        this.addItem = this.addItem.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
        this.onChangeFirstName = this.onChangeFirstName.bind(this);
        this.onChangeSurName = this.onChangeSurName.bind(this);
        this.saveIdItem = this.saveIdItem.bind(this);
        this.updateItem = this.updateItem.bind(this);
    }

    service = new getPostServices();

    state = {
        data: [],
        id: null,
        updateName: '',
        updateSurname: '',
        changeName: false,
        changeSurname: false, 
        loading: false,
        isCorrect: false,
        isError: false
    }

    upDateState() {
        this.service.getData()
            .then(data => {
                this.setState({
                    data,
                    loading: true
                });
            })
            .catch(() => {
                this.setState({isError: true});
            });
    }

    componentDidMount() {
        this.upDateState();
    }

    addItem(data) {
        let obj = {
            data: {
                name: data.name,
                surname: data.surname
            }
        }
        this.setState({ loading: false })
        this.service.postData(obj)
                    .then(() => {
                        this.setState({isCorrect: true})
                        this.upDateState();
                    })
                    .then(() => {
                        setTimeout(() => {
                            this.setState({isCorrect: false});
                        }, 1500)
                    })
                    .catch(() => {
                        this.setState({isError: true});
                    });
    }

    deleteItem(id) {
        this.setState({ loading: false })
        this.service.deleteItem(id)
                    .then(() => {
                        this.upDateState();
                    })
                    .catch(() => {
                        this.setState({isError: true});
                    });
    }

    saveIdItem(id) {
        this.setState({id});     
    }

    updateItem() {
        const {updateName, updateSurname, data, id, changeName, changeSurname} = this.state;

        let infoItem = {
            name: '',
            surname: ''
        }

        data.forEach(item => {
            if(id === item._id) {
                infoItem.name = item.data.name;
                infoItem.surname = item.data.surname
            }
        })
        
        let obj = {
            _id: id,
            data: {
                name: updateName,
                surname: updateSurname
            }
        }

        if(!changeName) {
            obj.data.name = infoItem.name
        }

        if(!changeSurname) {
            obj.data.surname = infoItem.surname
        }

        this.setState({ loading: false })
        this.service.updateItem(this.state.id, obj)
                    .then(() => {
                        this.setState({
                            id: null,
                            updateName: '',
                            updateSurname: '',
                            changeName: false,
                            changeSurname: false
                        });
                        this.upDateState();
                    })
                    .catch(() => {
                        this.setState({isError: true});
                    });
    }

    onChangeFirstName(event) {
        this.setState({
            updateName: event.target.value,
            changeName: true
        });
    }

    onChangeSurName(event) {
        this.setState({
            updateSurname: event.target.value,
            changeSurname: true
        }); 
    }

    createList = (arr) => {
        return arr.map(item => {
            const {_id, data} = item;
            const {name, surname} = data;

            if(_id === this.state.id) {
                return (
                    <tr key={_id}>
                        <td className="cellId">{_id}</td>
                        <td>
                            <Form.Control className="input-width" type="text" defaultValue={name} onInput={this.onChangeFirstName} />
                        </td>
                        <td className="position">
                            <Form.Control className="input-width" type="text" defaultValue={surname}  onInput={this.onChangeSurName} />
                            <div className="actions dop">
                                <Button type="submit" className="btn" variant="success" size="sm" onClick={this.updateItem} >Сохранить</Button>
                            </div>
                        </td>
                    </tr>
                )
            } else {
                return (
                    <tr key={_id}>
                        <td className="cellId">{_id}</td>
                        <td>{name}</td>
                        <td className="position">
                            {surname}
                            <div className="actions">
                                <Button type="button" className="btn" variant="outline-secondary" size="sm" onClick={() => this.saveIdItem(_id)} >{imgUpdate}</Button>
                                <Button type="button" className="btn" variant="outline-secondary" size="sm" onClick={() => this.deleteItem(_id)} >{imgDelete}</Button>
                            </div>
                        </td>
                    </tr>
                )
            }
        })
    }

    render() {
        const {data, loading, isCorrect, isError} = this.state;

        let elements;

        let successMessage = isCorrect ? <Alert className="margin" variant="success">Успешно отправлено!</Alert> : null;
        let dangerMessage = isError ? <Alert className="margin" variant="danger">Упс! Что-то пошло не так... Попробуйте позже</Alert> : null;

        if(!loading) {
            return (
                <Spinner className="margin" animation="border" variant="primary" />
            )
        }

        if(data.length > 0) {
            elements = this.createList(data);
        }

      return (
        <div className="Body">
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Surname</th>
                    </tr>
                </thead>
                <tbody>
                    {elements}
                </tbody>
            </Table>
            {dangerMessage}
            <PostAddForm addItem={this.addItem} />
            {successMessage}
        </div>
      );
    }
}
