import React, { Component } from 'react';
import Body from '../body/Body'
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Body></Body>
      </div>
    );
  }
}

export default App;
